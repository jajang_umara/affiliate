<div class="sl-logo"><a href=""><i class="icon ion-android-star-outline"></i> starlight</a></div>
<div class="sl-sideleft">
	<div class="input-group input-group-search">
		<input type="search" name="search" class="form-control" placeholder="Search">
		<span class="input-group-btn">
			<button class="btn"><i class="fa fa-search"></i></button>
		</span><!-- input-group-btn -->
	</div><!-- input-group -->

	<label class="sidebar-label">Navigation</label>
	<div class="sl-sideleft-menu">
		<a href="index.html" class="sl-menu-link <?php if (!$this->uri->segment(1)) : ?> active <?php endif; ?>">
			<div class="sl-menu-item">
				<i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
				<span class="menu-item-label">Dashboard</span>
			</div><!-- menu-item -->
		</a><!-- sl-menu-link -->
	 
		<a href="#" class="sl-menu-link <?php if ($this->uri->segment(1) == 'affiliate'){echo 'active show-sub';} ?>">
			<div class="sl-menu-item">
				<i class="menu-item-icon fa fa-user tx-20"></i>
				<span class="menu-item-label">Affiliate</span>
				<i class="menu-item-arrow fa fa-angle-down"></i>
			</div><!-- menu-item -->
		</a><!-- sl-menu-link -->
		<ul class="sl-menu-sub nav flex-column">
			<li class="nav-item"><a href="<?= site_url('affiliate'); ?>" class="nav-link <?php if ($this->uri->segment(1) == 'affiliate') : ?> active <?php endif; ?>">Data Affiliate</a></li>
			<li class="nav-item"><a href="<?= site_url('affiliate/add/0'); ?>" class="nav-link">Add Affiliate</a></li>
			
		</ul>
		<a href="#" class="sl-menu-link <?php if ($this->uri->segment(1) == 'referral'){echo 'active show-sub';} ?>">
			<div class="sl-menu-item">
				<i class="menu-item-icon fa fa-user tx-20"></i>
				<span class="menu-item-label">Referral</span>
				<i class="menu-item-arrow fa fa-angle-down"></i>
			</div><!-- menu-item -->
		</a><!-- sl-menu-link -->
		<ul class="sl-menu-sub nav flex-column">
			<li class="nav-item"><a href="<?= site_url('referral'); ?>" class="nav-link <?php if ($this->uri->segment(1) == 'referral') : ?> active <?php endif; ?>">Data Referral</a></li>
			<li class="nav-item"><a href="<?= site_url('referral/add/0'); ?>" class="nav-link">Add Referral</a></li>
			
		</ul>
		<a href="<?= site_url('transaction'); ?>" class="sl-menu-link <?php if ($this->uri->segment(1) == 'transaction') : ?> active <?php endif; ?>">
			<div class="sl-menu-item">
				<i class="menu-item-icon icon fa fa-table tx-22"></i>
				<span class="menu-item-label">Transaction</span>
			</div><!-- menu-item -->
		</a><!-- sl-menu-link -->
	</div>

	<br>
</div>