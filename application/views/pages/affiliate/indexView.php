<nav class="breadcrumb sl-breadcrumb">
	<a class="breadcrumb-item" href="<?= base_url(); ?>">Home</a>
	<span class="breadcrumb-item active">Affiliate</span>
</nav>

<div class="sl-pagebody">
	<div class="sl-page-title">
		<h5>Data Affiliate</h5>
		
	</div><!-- sl-page-title -->
	
	  <div class="card pd-20 pd-sm-40">
			<h6 class="card-body-title">Your Affiliates List</h6>
			

			<div class="table-wrapper">
				<table id="datatable" class="table display responsive nowrap">
					<thead>
						<tr>
							<th class="wd-10p">#</th>
							<th class="wd-15p">Fullname</th>
							<th class="wd-15p">Email</th>
							<th class="wd-20p">Paypal Email</th>
							<th class="wd-15p">Username</th>
							<th class="wd-10p">Country</th>
							<th class="wd-25p">Action</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
</div>


<script src="<?= base_url('assets/lib/highlightjs/highlight.pack.js'); ?>"></script>
<script src="<?= base_url('assets/lib/datatables/jquery.dataTables.js'); ?>"></script>
<script src="<?= base_url('assets/lib/datatables-responsive/dataTables.responsive.js'); ?>"></script>


<script>
	var table;
	$(function(){
		table = $('#datatable').DataTable({
						responsive: true,
						processing: true,
						serverSide: true,
						language: {
							searchPlaceholder: 'Search...',
							sSearch: '',
						},
						ajax: {
							type: 'POST',
							url: "<?= site_url('affiliate/data'); ?>", 
							data: function ( data ) {
								data.nama = $('#nama').val();
								data.role = $('#role').val();
							}
						},
						columns: [
							{ "data": null,sortable:false },
							{ "data": "fullname" },
							{ "data": "email" },
							{ "data": "paypal_email" },
							{ "data": "username" },
							{ "data": "country" },
							{ "data": null,render:function(data,type,row){
								return '<a class="btn btn-warning btn-sm" href="<?= site_url('affiliate/add'); ?>/'+row.id+'" title="Edit Affiliate"><i class="fa fa-edit"></i></a>';
							} }
						],
						"fnCreatedRow": function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); },
					});
		
		$('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
	});
</script>