<nav class="breadcrumb sl-breadcrumb">
	<a class="breadcrumb-item" href="<?= base_url(); ?>">Home</a>
	<a class="breadcrumb-item" href="<?= site_url('affiliate'); ?>">Affiliates</a>
	<span class="breadcrumb-item active">Add New</span>
</nav>

<div class="sl-pagebody">
	<div class="sl-page-title">
		<h5>Add New Affiliate</h5>
		<p>Enter complete detail to add new affiliate.</p>
	</div><!-- sl-page-title -->

	<div class="card pd-20 pd-sm-20">
		<div class="card pd-20 pd-sm-20 form-layout form-layout-4">
			<form id="affiliate-form">
				
				<div class="row row-sm mg-t-10">
					<div class="col-xl-6">
						<p>Basic Detail.</p>
						<div class="row">
							<label class="col-sm-4 form-control-label">Fullname: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<input type="text" class="form-control" placeholder="Enter fullname" name="fullname" required value="<?= @$row->fullname; ?>">
							</div>
						</div>
						<div class="row mg-t-20">
							<label class="col-sm-4 form-control-label">Country: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<div id="slWrapper" class="parsley-select">
									<select name="country" class="form-control select2" required data-parsley-class-handler="#slWrapper" data-parsley-errors-container="#slErrorContainer">
										<option value="">-Select Country-</option>
										<?php foreach($countries as $key=>$country) : ?>
											<option value="<?= $country; ?>" <?php if(isset($row) && $row->country == $country){echo 'selected';} ?>><?= $country; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div id="slErrorContainer"></div>
							</div>
						</div>
						<div class="row mg-t-20">
							<label class="col-sm-4 form-control-label">Email: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<input type="email" class="form-control" placeholder="Enter email" name="email" required value="<?= @$row->email; ?>">
							</div>
						</div>
						<div class="row mg-t-20">
							<label class="col-sm-4 form-control-label">Paypal Email: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<input type="email" class="form-control" placeholder="Enter Paypal Email" name="paypal_email" required value="<?= @$row->paypal_email; ?>">
							</div>
						</div>
					</div>
					<div class="col-xl-6">
						<p>Login Detail.</p>
						<div class="row">
							<label class="col-sm-4 form-control-label">Usename: <span class="tx-danger">*</span>
								
							</label>
							
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<input type="text" class="form-control" placeholder="Enter username" name="username" maxlength="11" required data-parsley-maxlength="11" value="<?= @$row->username; ?>" <?= isset($row) ? 'readonly' : '';?>>
								<small id="emailHelp" class="form-text text-muted">Maks 11 characters.</small>
							</div>
						</div>
						<div class="row mg-t-20">
							<label class="col-sm-4 form-control-label">Password: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<input type="password" class="form-control" placeholder="Enter fullname" <?= isset($row) ? '' :'required'; ?> name="password" id="password">
							</div>
						</div>
						<div class="row mg-t-20">
							<label class="col-sm-4 form-control-label">Repeat Password: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<input type="password" class="form-control" placeholder="Enter fullname" name="repeat_password" <?= isset($row) ? '' :'required'; ?> data-parsley-equalto="#password">
							</div>
						</div>
						
						<div class="row mg-t-20">
							<?php if(!isset($row)) : ?>
							<label class="ckbox mg-l-20">
								<input type="checkbox" name="send_email" value="1">
								<span>Send Affiliate account creation email notification ?</span>
							</label>
							<?php endif; ?>
						</div>
						
						<div class="form-layout-footer mg-t-30">
							<button class="btn btn-info mg-r-5" type="submit" id="submit">Submit Form</button>
							<button class="btn btn-secondary" type="button">Cancel</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
 <script src="<?= base_url('assets/lib/parsleyjs/parsley.js'); ?>"></script>
<script>
	$(function(){
		$('.select2').select2();
		
		$('#affiliate-form').parsley();
		
		$('#affiliate-form').on('submit',function(e){
			e.preventDefault();
			
			beforeSubmit();
			$.post("<?= site_url('affiliate/store/'.$id); ?>",$(this).serialize()).done(function(response){
				if (response == 'success'){
					Swal.fire("Your Affiliate has been saved");
					$('#affiliate-form')[0].reset();
				} else {
					Swal.fire(response);
				}
				afterSubmit();
				
			}).fail(function(xhr){
				Swal.fire(xhr.responseText)
				afterSubmit();
			})
		})
	})
	
	function beforeSubmit()
	{
		$('#submit').prop('disabled',true);
		$('#submit').text('Saving...');
	}
	
	function afterSubmit()
	{
		$('#submit').prop('disabled',false);
		$('#submit').text('Submit Form');
		
	}
</script>