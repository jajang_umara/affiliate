<nav class="breadcrumb sl-breadcrumb">
	<a class="breadcrumb-item" href="<?= base_url(); ?>">Home</a>
	<span class="breadcrumb-item active">Transaction</span>
</nav>

<div class="sl-pagebody">
	<div class="sl-page-title">
		<h5>Transaction</h5>
		
	</div><!-- sl-page-title -->
	
	  <div class="card pd-20 pd-sm-40">
			<div class="card-header card-header-default bg-gray-400 justify-content-between">
            <h6 class="mg-b-0 tx-14 tx-inverse">Referral Transaction List</h6>
               <div class="card-option tx-12">
						<a href="#" class="tx-gray-600 mg-l-10" onclick="uploadTransaction()"><i class="icon fa fa-upload"></i> Upload Transaction</a>
                  
               </div><!-- card-option -->
         </div><!-- card-header -->
			<div class="card-body">
				<div class="table-wrapper">
					<table id="datatable" class="table display responsive nowrap">
						<thead>
							<tr>
								<th class="wd-10p">#</th>
								<th class="wd-20p">Shop URL</th>
								<th class="wd-20p">Plan</th>
								<th class="wd-20p">Charge Date</th>
								<th class="wd-20p">Charge Amount</th>
								<th class="wd-20p">Affiliate Fee</th>
								<th class="wd-20p">Affiliate Username</th>
								<th class="wd-25p">Action</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
</div>

 <!-- LARGE MODAL -->
        <div id="myModal" class="modal fade">
          <div class="modal-dialog" role="document">
            <div class="modal-content tx-size-sm">
              <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Upload Transaction</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body pd-20">
					<div class="card pd-20 pd-sm-40 mg-t-10">
						<div class="row">
							<div class="col-lg-12 mg-t-40 mg-lg-t-0">
								<label class="custom-file">
									<input type="file" id="file" class="custom-file-input" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" onchange="$('.custom-file-control').text('1 file selected')">
									<span class="custom-file-control custom-file-control-primary"></span>
								</label>
							</div>
						</div>
					</div>
              </div><!-- modal-body -->
              <div class="modal-footer">
                <button type="button" class="btn btn-info pd-x-20" onclick="doUpload()">Upload</button>
                <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div><!-- modal-dialog -->
        </div><!-- modal -->


<script src="<?= base_url('assets/lib/highlightjs/highlight.pack.js'); ?>"></script>
<script src="<?= base_url('assets/lib/datatables/jquery.dataTables.js'); ?>"></script>
<script src="<?= base_url('assets/lib/datatables-responsive/dataTables.responsive.js'); ?>"></script>


<script>
	var table;
	$(function(){
		table = $('#datatable').DataTable({
						responsive: true,
						processing: true,
						serverSide: true,
						language: {
							searchPlaceholder: 'Search...',
							sSearch: '',
						},
						ajax: {
							type: 'POST',
							url: "<?= site_url('transaction/data'); ?>", 
							data: function ( data ) {
								data.nama = $('#nama').val();
								data.role = $('#role').val();
							}
						},
						columns: [
							{ "data": null,sortable:false },
							{ "data": "shop_url" },
							{ "data": "plan" },
							{ "data": "transaction_date" },
							{ "data": "transaction_amount" },
							{ "data": "affiliate_fee" },
							{ "data": "affiliate_id" },
							{ "data": null,render:function(data,type,row){
								return '<a class="btn btn-danger btn-sm" href="#" title="Delete Transaction" onclick="deleteRow('+row.id+')"><i class="fa fa-trash"></i></a>';
							} }
						],
						"fnCreatedRow": function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); },
					});
		
		$('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
	});
	
	function uploadTransaction()
	{
		$('#myModal').modal('show')
	}
	
	function doUpload()
	{
		var formData = new FormData();
		formData.append('file', $('#file')[0].files[0]);

		$.ajax({
			 url : '<?= site_url("transaction/upload"); ?>',
			 type : 'POST',
			 data : formData,
			 processData: false,  // tell jQuery not to process the data
			 contentType: false,  // tell jQuery not to set contentType
			 success : function(data) {
				if (data == 'success'){
					table.draw();
					Swal.fire("Success","Success Uploaded Data",'success');
				} else {
					Swal.fire(data);
				}
				
			 }
		});
	}
	
	function deleteRow(id)
	{
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		  if (result.value) {
			  $.post("<?= site_url('transaction/delete'); ?>",{id:id}).done(function(result){
				  if (result == 'success'){
					  table.draw()
					   Swal.fire(
							'Deleted!',
							'Your data has been deleted.',
							'success'
						)
				  } else {
					  Swal.fire('Error','Error Deleted Data','error')
				  }
			  })
			
		  }
		})
	}
</script>