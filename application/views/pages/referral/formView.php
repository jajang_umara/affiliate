<nav class="breadcrumb sl-breadcrumb">
	<a class="breadcrumb-item" href="<?= base_url(); ?>">Home</a>
	<a class="breadcrumb-item" href="<?= site_url('affiliate'); ?>">Affiliates</a>
	<span class="breadcrumb-item active">Add New</span>
</nav>

<div class="sl-pagebody">
	<div class="sl-page-title">
		<h5><?= isset($row) ? 'Edit Referral' : 'Add New Referral'; ?></h5>
		<p>Enter complete detail to add new affiliate Referral.</p>
	</div><!-- sl-page-title -->

	<div class="card pd-20 pd-sm-20">
		<div class="card pd-20 pd-sm-20 form-layout form-layout-4">
			<form id="referral-form">
				
				<div class="row row-sm mg-t-10">
					<div class="col-xl-6">
						
						<div class="row">
							<label class="col-sm-4 form-control-label">Affiliate username: <span class="tx-danger">*</span></label>
							
								<div class="col-sm-8 mg-t-10 mg-sm-t-0">
									<div id="slWrapper2" class="parsley-select">
										<select name="affiliate_id" class="form-control select2" required data-parsley-class-handler="#slWrapper2" data-parsley-errors-container="#slErrorContainer2">
											<option value="">-Select Affiliate username-</option>
											<?php if($affiliates) : foreach($affiliates as $key=>$aff) : ?>
												<option value="<?= $key; ?>" <?php if(isset($row) && $row->affiliate_id == $key){echo 'selected';} ?>><?= $aff; ?></option>
											<?php endforeach; endif; ?>
										</select>
									</div>
									<div id="slErrorContainer2"></div>
							</div>
							
						</div>
						<div class="row mg-t-20">
							<label class="col-sm-4 form-control-label">Country: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<div id="slWrapper" class="parsley-select">
									<select name="country" class="form-control select2" required data-parsley-class-handler="#slWrapper" data-parsley-errors-container="#slErrorContainer">
										<option value="">-Select Country-</option>
										<?php foreach($countries as $key=>$country) : ?>
											<option value="<?= $country; ?>" <?php if(isset($row) && $row->country == $country){echo 'selected';} ?>><?= $country; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div id="slErrorContainer"></div>
							</div>
						</div>
						<div class="row mg-t-20">
							<label class="col-sm-4 form-control-label">Email: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<input type="email" class="form-control" placeholder="Enter email" name="email" required value="<?= @$row->email; ?>">
							</div>
						</div>
						<div class="row mg-t-20">
							<label class="col-sm-4 form-control-label">Shop URL: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<input type="text" class="form-control" placeholder="Enter Shop Url" name="shop_url" required value="<?= @$row->shop_url; ?>">
							</div>
						</div>
						<div class="row mg-t-20">
							<?php if(!isset($row)) : ?>
							<label class="ckbox mg-l-20">
								<input type="checkbox" name="send_email" value="1">
								<span>Send Affiliate Referral account creation email notification ?</span>
							</label>
							<?php endif; ?>
						</div>
						<div class="form-layout-footer mg-t-30">
							<button class="btn btn-info mg-r-5" type="submit" id="submit">Submit Form</button>
							<button class="btn btn-secondary" type="button">Cancel</button>
						</div>
					</div>
					<div class="col-xl-6">
						<div class="row">
							<label class="col-sm-4 form-control-label">Status: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<select name="status" class="form-control" required>
									<option value="">-Select Status-</option>
									<?php foreach(getStatus() as $stats) : ?>
									<option <?php if(isset($row) && $row->status == $stats){echo 'selected';} ?>><?= $stats; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="row mg-t-20">
							<label class="col-sm-4 form-control-label">Plan: <span class="tx-danger">*</span></label>
							<div class="col-sm-8 mg-t-10 mg-sm-t-0">
								<select name="plan" class="form-control" required>
									<option value="">-Select Plan-</option>
									<?php foreach( getPlan() as $plan) : ?>
									<option <?php if(isset($row) && $row->plan == $plan){echo 'selected';} ?>><?= $plan; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						
					
						
						
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
 <script src="<?= base_url('assets/lib/parsleyjs/parsley.js'); ?>"></script>
<script>
	$(function(){
		$('.select2').select2();
		
		$('#referral-form').parsley();
		
		$('#referral-form').on('submit',function(e){
			e.preventDefault();
			
			beforeSubmit();
			$.post("<?= site_url('referral/store/'.$id); ?>",$(this).serialize()).done(function(response){
				if (response == 'success'){
					Swal.fire("Your Affiliate Referral has been saved");
					$('#referral-form')[0].reset();
				} else {
					Swal.fire(response);
				}
				afterSubmit();
			}).fail(function(xhr){
				Swal.fire(xhr.responseText);
				afterSubmit()
			})
		})
	})
	
	function beforeSubmit()
	{
		$('#submit').prop('disabled',true);
		$('#submit').text('Saving...');
	}
	
	function afterSubmit()
	{
		$('#submit').prop('disabled',false);
		$('#submit').text('Submit Form');
		
	}
</script>