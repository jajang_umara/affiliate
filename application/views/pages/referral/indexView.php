<nav class="breadcrumb sl-breadcrumb">
	<a class="breadcrumb-item" href="<?= base_url(); ?>">Home</a>
	<span class="breadcrumb-item active">Referral</span>
</nav>

<div class="sl-pagebody">
	<div class="sl-page-title">
		<h5>Referral</h5>
		
	</div><!-- sl-page-title -->
	
	  <div class="card pd-20 pd-sm-40">
			<h6 class="card-body-title">Your Referral List</h6>
			<div class="row">
				<div class="col-md-3">
					<select class="form-control" id="status" onchange="doFilter()">
						<option value="">Filter By Status</option>
						<?php foreach(getStatus() as $stats) : ?>
						<option><?= $stats; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-md-3">
					<select class="form-control" id="affiliate" onchange="doFilter()">
						<option value="">Filter By Affiliate</option>
						<?php foreach(getAffiliate() as $aff) : ?>
						<option value="<?= $aff->id; ?>"><?= $aff->username; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="table-wrapper">
				<table id="datatable" class="table display responsive nowrap">
					<thead>
						<tr>
							<th class="wd-10p">#</th>
							<th class="wd-15p">Username</th>
							<th class="wd-15p">Email</th>
							<th class="wd-20p">Shop URL</th>
							<th class="wd-15p">Country</th>
							<th class="wd-10p">Status</th>
							<th class="wd-10p">Plan</th>
							<th class="wd-25p">Action</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table>
						<tr>
							<td>Summary</td>
							<td></td>
							<td>Active shops summary</td>
							<td></td>
						</tr>
						<tr>
							<td>Total shops </td>
							<td>: <?= $total; ?></td>
							<td>Lite</td>
							<td>: <?= $Lite; ?></td>
						</tr>
						<tr>
							<td>Trial shops </td>
							<td>: <?= $Pending; ?></td>
							<td>Basic</td>
							<td>: <?= $Basic; ?></td>
						</tr>
						<tr>
							<td>Active shops </td>
							<td>: <?= $Active; ?></td>
							<td>Professional</td>
							<td>: <?= $Professional; ?></td>
						</tr>
						<tr>
							<td>Inactive shops</td>
							<td>: <?= $Suspended; ?></td>
							<td>Ultimate</td>
							<td>: <?= $Ultimate; ?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
</div>


<script src="<?= base_url('assets/lib/highlightjs/highlight.pack.js'); ?>"></script>
<script src="<?= base_url('assets/lib/datatables/jquery.dataTables.js'); ?>"></script>
<script src="<?= base_url('assets/lib/datatables-responsive/dataTables.responsive.js'); ?>"></script>


<script>
	var table;
	$(function(){
		table = $('#datatable').DataTable({
						responsive: true,
						processing: true,
						serverSide: true,
						language: {
							searchPlaceholder: 'Search...',
							sSearch: '',
						},
						ajax: {
							type: 'POST',
							url: "<?= site_url('referral/data'); ?>", 
							data: function ( data ) {
								data.status = $('#status').val();
								data.affiliate = $('#affiliate').val();
							}
						},
						columns: [
							{ "data": null,sortable:false },
							{ "data": "username" },
							{ "data": "email" },
							{ "data": "shop_url" },
							{ "data": "country" },
							{ "data": "status" },
							{ "data": "plan" },
							{ "data": null,render:function(data,type,row){
								return '<a class="btn btn-warning btn-sm" href="<?= site_url('referral/add'); ?>/'+row.id+'" title="Edit Affiliate"><i class="fa fa-edit"></i></a>';
							} }
						],
						"fnCreatedRow": function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); },
					});
		
		$('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
	});
	
	function doFilter()
	{
		table.draw();
	}
</script>