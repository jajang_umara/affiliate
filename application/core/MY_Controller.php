<?php

class MY_Controller extends CI_Controller
{
	function render($view,$data=[])
	{
		$data['content'] = $this->load->view($view,$data,true);
		$this->load->view('template',$data);
	}
	
	function post($index,$default='')
	{
		return isset($_POST[$index]) ? $this->input->post($index,true) : $default;
	}
	
	function get($index,$default='')
	{
		return isset($_GET[$index]) ? $this->input->get($index,true) : $default;
	}
	
	function send_mail($to,$subject,$message,$attach=false)
  {
		$this->load->library('email');
		$config = array();
		$config['protocol'] 	= 'smtp';
		$config['smtp_host'] 	= 'mail.crf-lac.com';
		$config['smtp_user'] 	= 'no-reply@crf-lac.com';
		$config['smtp_pass'] 	= 'Tetran12345';
		$config['smtp_port'] 	= 25;
		$config['charset']    = 'utf-8';
		$config['newline']    = "\r\n";
		$config['mailtype'] 	= 'html';
		$this->email->initialize($config);

		$from = "no-reply@affiliate.com";
		$this->email->from($from, 'No Reply');
		$this->email->to($to);

		$this->email->subject($subject);
		$this->email->message($message);

		if ($attach != false){
			$this->email->attach($attach);
		}

		if($this->email->send())
		{
				//echo "Mail Sent Successfully";
		}
		else
		{
				echo "Failed to send email";
				show_error($this->email->print_debugger());             
		}
  }
}