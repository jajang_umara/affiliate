<?php

class ReferralController extends MY_Controller
{
	function index()
	{
		foreach(['Pending','Active','Suspended'] as $status){
			$data[$status] = $this->referral->total_shops($status);
		}
		foreach (getPlan() as $plan){
			$data[$plan] = $this->referral->total_shops('',$plan);
		}
		
		$data['total'] = $this->referral->count();
		$this->render('pages/referral/indexView',$data);
	}
	
	function data()
	{
		echo json_encode($this->referral->datatable());
	}
	
	function add($id)
	{
		$countries = $this->countries->dropdown('id','country_name');
		$affiliates = $this->affiliate->dropdown('id','username');
		$row = $this->referral->get($id);
		$this->render('pages/referral/formView',compact('countries','id','row','affiliates'));
	}
	
	function store($id)
	{
		$data = [
			'affiliate_id' => $this->post('affiliate_id'),
			'shop_url' => $this->post('shop_url'),
			'email' => $this->post('email'),
			'status' => $this->post('status'),
			'country' => $this->post('country'),
			'plan' => $this->post('plan')
		];
		
		if ($this->referral->upsert($data,$id)){
			echo 'success';
		} else {
			echo 'Failed Save or edit Affiliate data';
		}
	}
}