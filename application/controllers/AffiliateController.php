<?php

class AffiliateController extends MY_Controller
{
	function index()
	{
		$this->render('pages/affiliate/indexView');
	}
	
	function data()
	{
		echo json_encode($this->affiliate->datatable());
	}
	
	function add($id)
	{
		$countries = $this->countries->dropdown('id','country_name');
		$row = $this->affiliate->get($id);
		$this->render('pages/affiliate/formView',compact('countries','id','row'));
	}
	
	function store($id)
	{
		$data = [
			'username' => $this->post('username'),
			'fullname' => $this->post('fullname'),
			'role'	=> 'user',
			'email' => $this->post('email'),
			'paypal_email' => $this->post('paypal_email'),
			'country' => $this->post('country'),
			'password' => sha1($this->post('password'))
		];
		
		if ($data['password'] == ''){
			unset($data['password']);
		}
		
		if ($this->affiliate->upsert($data,$id)){
			echo 'success';
		} else {
			echo 'Failed Save or edit Affiliate data';
		}
	}
}