<?php

class TransactionController extends MY_Controller
{
	function index()
	{
		$this->render('pages/transaction/indexView');
	}
	
	function data()
	{
		echo json_encode($this->transaction->datatable());
	}
	
	function upload()
	{
		if (isset($_FILES['file'])){
			$filename = $_FILES['file']['name'];
			$ext = explode('.',$filename);
			if (strtolower(end($ext)) == 'csv'){
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';
				$excelreader     = new PHPExcel_Reader_Csv();
				$loadexcel         = $excelreader->load($_FILES['file']['tmp_name']); // Load file yang telah diupload ke folder excel
				$sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
				if (count($sheet) > 0){
					//print_r($sheet); exit();
					foreach($sheet as $k=>$r){
						
						if ($k == 1) continue;
						$data = [
							'transaction_date' => date('Y-m-d H:i:s',strtotime($r['B'])),
							'shop_url' => $r['C'],
							'category' => $r['G'],
							'transaction_amount' => $r['G'] == 'App revenue' ? $r['H'] : 0,
							'transaction_title' => $r['I'],
							
						];
						
						$data['affiliate_fee'] = $data['transaction_amount']*0.12;
						
						if ($this->transaction->where('shop_url',$r['C'])->where('transaction_date',$data['transaction_date'])->count() == 0)
						{
							$this->transaction->insert_transaction($data);
						}
					}
					
					echo 'success';
				}
			} else {
				echo 'Not Csv File';
			}
		}


		
	}
	
	function delete()
	{
		$id = $this->post('id');
		if ($this->transaction->delete($id)){
			echo 'success';
		}
	}
}