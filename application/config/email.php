<?php
$config['email_template'] ='
<!DOCTYPE html PUBLIC " -//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <style type="text/css">body, html {
        width: 100% !important;
        margin: 0;
        padding: 0;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none;
        -ms-text-size-adjust: 100%;
    }
    table td, table {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
    }
    #outlook a {
        padding: 0;
    }
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
        line-height: 100%;
    }
    .ExternalClass {
        width: 100%;
    }
    @media only screen and (max-width: 480px) {
        table {
            width: 100% !important;
        }
        img {
            width: inherit;
        }
        .layer_2 {
            max-width: 100% !important;
        }
    }
</style>
</head>
<body style="padding:0; margin: 0;">
    <table style="height: 100%; width: 100%; background-color: #efefef;" align="center">
        <tbody>
            <tr>
                <td valign="top" id="dbody" data-version="2.30" style="width: 100%; height: 100%; padding-top: 30px; padding-bottom: 30px; background-color: #efefef;">
                    
                    <table class="layer_1" align="center" border="0" cellpadding="0" cellspacing="0" style="max-width: 600px; box-sizing: border-box; width: 100%; margin: 0px auto;">
                        <tbody>
                            <tr>
                                <td class="drow" valign="top" style="background-color: rgb(115, 0, 0); box-sizing: border-box; font-size: 0px; text-align: center;">
                                    
                                    <div class="layer_2" style="display: inline-block; vertical-align: top; width: 100%; max-width: 600px;">
                                        <table border="0" cellspacing="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edtext" style="padding: 20px; text-align: right; color: #5f5f5f; font-size: 14px; font-family: Helvetica, Arial, sans-serif; word-break: break-word; direction: ltr; box-sizing: border-box;">
                                                        <p style="margin: 0px; padding: 0px;">
                                                            <img width="30%" src="'.base_url('assets/img/APPS.png').'">
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </td>
                            </tr>

                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    
                                    <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 10px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    
                                    <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edimg" style="padding: 0px; box-sizing: border-box; text-align: center;">
                                                        <img src="https://api.elasticemail.com/userfile/12301ee9-d04b-4adb-84db-0e43666eaa22/geometric_divider1.png" alt="Image" width="576" style="border-width: 0px; border-style: none; max-width: 576px; width: 100%;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    
                                    <div class="layer_2" style="display: inline-block; vertical-align: top; width: 100%; max-width: 600px;">
                                        <table border="0" cellspacing="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edtext" style="padding: 20px; text-align: left; color: #5f5f5f; font-size: 14px; font-family: Helvetica, Arial, sans-serif; word-break: break-word; direction: ltr; box-sizing: border-box;">
                                                        <p style="margin: 0px; padding: 0px;">
                                                            {CONTENT}
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </td>
                            </tr>
                        
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    
                                    <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 10px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                </td>
            </tr>
        </tbody>
    </table>

    <a href="{unsubscribe}"></a>

</body>
</html>
';