<?php

class Referral extends MY_Model
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'referral';
		$this->column_order = ['username','email','shop_url','status','plan'];
		$this->column_search = ['email','shop_url'];
		$this->order = array('id' => 'asc');
	}
	
	function datatable()
	{
		$select = "$this->table.*,users.username";
		$join   = [["users","users.id=$this->table.affiliate_id"]];
		$rows = $this->_get_datatable($select,$join);
		return $rows;
	}
	
	function filter()
	{
		$this->db->like('status',$_POST['status']);
		if (isset($_POST['affiliate']) && $_POST['affiliate'] != ''){
			$this->db->where('affiliate_id',$_POST['affiliate']);
		}
	}
	
	function total_shops($status,$plan=null)
	{
		if ($plan != null){
			return $this->where('status','Active')->where('plan',$plan)->count();
		}
		return $this->where('status',$status)->count();
	}
}