<?php

class Affiliate extends MY_Model
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'users';
		$this->column_order = ['fullname','email','paypal_email','username','country'];
		$this->column_search = ['fullname','email','paypal_email','username','country'];
		$this->order = array('id' => 'asc');
	}
	
	function datatable()
	{
		$rows = $this->_get_datatable();
		
		return $rows;
	}
	
	
}