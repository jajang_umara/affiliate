<?php

class Transaction extends MY_Model
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'transaction';
		$this->column_order = ['shop_url','referral.plan','transaction_date'];
		$this->column_search = ['shop_url'];
		$this->order = array('id' => 'asc');
	}
	
	function datatable()
	{
		$select = "$this->table.*,referral.plan,referral.affiliate_id";
		$join   = [["referral","referral.shop_url=$this->table.shop_url"]];
		$rows = $this->_get_datatable($select,$join);
		return $rows;
	}
	
	function insert_transaction($data)
	{
		$this->db->query("INSERT INTO transaction (shop_url,transaction_date,category,transaction_amount,transaction_title,affiliate_fee)
								SELECT '".$data['shop_url']."','".$data['transaction_date']."','".$data['category']."',".$data['transaction_amount'].",
								'".$data['transaction_title']."',".$data['affiliate_fee']." WHERE EXISTS (SELECT 1 FROM referral WHERE shop_url = '".$data['shop_url']."')");
	}
}