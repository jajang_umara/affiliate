<?php

function _this()
{
	$ci =& get_instance();
	return $ci;
}

function getStatus()
{
	return ['Pending','Active','Cancelled','Suspended'];
}

function getPlan()
{
	return ['Lite','Basic','Professional','Ultimate'];
}

function getAffiliate()
{
	return _this()->db->get('users')->result();
}